package purse;

import java.util.Scanner;

public class Purse {
	float bal;
	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		Purse p=new Purse();
		int op;
		char ch;
		float cash;
		do {
			System.out.println("MENU\n");
			System.out.println("1.ADD\n2.Withdraw\n3.Bill\n4.Exit\n");
			System.out.println("\nSelect your option : ");
			op = input.nextInt();
			switch(op) {
				case 1: System.out.println("\nEnter the amount to be added : ");
						cash = input.nextFloat();
						p.addMoney(cash);
						break;
				case 2: System.out.println("\nEnter the amount to be withdrawn : ");
						cash = input.nextFloat();
						p.withdraw(cash);
						break;
				case 3:	p.printBill();
						break;
				case 4:
				default:break;
			}
			System.out.println("Do you wish to continue? : ");
			ch=input.next().charAt(0);
			
		}while(ch=='y');	
	}
	public void addMoney(float cash) {
		bal += cash;
	}
	public void withdraw(float cash) {
		bal -= cash;
	}
	public void printBill() {
		System.out.println("\nBILL\n--------------");
		System.out.println("Balance Amount : "+ bal);
	}
}
